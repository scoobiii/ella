## Ella: Uma Linguagem de Programação Mágica ✨

**Conceito:**

Ella é uma linguagem de programação mágica e uma comunidade vibrante que celebra a diversidade em todas as suas formas , unindo a tecnologia à riqueza da expressão humana ️. Ela é uma homenagem às origens 🪶, reconhecendo a sabedoria e o conhecimento ancestrais presentes em diferentes culturas e comunidades.

**Características:**

* **Multilíngue e Multissignificativa:** O código pode ser escrito em qualquer idioma , incluindo línguas indígenas e dialetos regionais ️. Cada palavra ou frase pode ter múltiplos significados e interpretações , adicionando profundidade e nuance aos seus programas .
* **Inclusiva e Diversificada:** A comunidade acolhe a todos, independentemente de sua origem , identidade de gênero ️‍ ou expressão ️. A própria linguagem evolui através do aprendizado colaborativo  e incorpora diversas perspectivas culturais .
* **Código Significativo:** O código não é apenas uma sequência de comandos; é um reflexo de sua intenção criativa  e background cultural . Os programas podem ser infundidos com música , poesia ️ e expressões idiomáticas locais ️, tornando-os verdadeiramente únicos e expressivos ✨.
* **Acessível e Empoderadora:** Ella usa metáforas mágicas intuitivas ✨ e storytelling ️ para tornar a programação acessível a todos ‍‍. Tutoriais e recursos estão disponíveis em diversos formatos , promovendo um ambiente de aprendizado de apoio .
* **Impacto no Mundo Real:** A comunidade usa Ella para criar projetos significativos  que abordam desafios sociais e ambientais , celebrando diversas vozes ️ e promovendo a compreensão cultural .

**Estética Visual:**

Imagine uma aldeia global vibrante , onde pessoas de todas as culturas se reúnem para compartilhar seus conhecimentos  e criar juntos ✨. A linguagem Ella é tecida com símbolos  e elementos de diferentes culturas , criando uma tapeçaria rica e colorida .

**Exemplos de Uso:**

* Uma curandeira tradicional ‍⚕️ pode codificar seu conhecimento medicinal  em Ella, criando um programa que ajuda a diagnosticar e tratar doenças 🩺.
* Um agricultor ‍ pode usar Ella para automatizar tarefas agrícolas , incorporando conhecimentos ancestrais de agricultura sustentável .
* Uma artista ‍ pode criar uma instalação de arte interativa ️ usando Ella, combinando tecnologia  com elementos da cultura local .

**Possibilidades:**

As possibilidades de Ella são infinitas ♾️. É uma ferramenta poderosa  que pode ser usada para criar coisas bonitas ✨, úteis ️ e inspiradoras . O futuro da tecnologia está em nossas mãos , e Ella nos dá o poder de moldá-lo de forma inclusiva  e diversa .

**Homenagem à Origem:**

Ella é uma homenagem às origens 🪶, reconhecendo a sabedoria e o conhecimento ancestrais presentes em diferentes culturas e comunidades. A linguagem incorpora elementos de diversas culturas, como:

* **Símbolos e glifos:** A linguagem Ella pode usar símbolos  e glifos 🪶 de diferentes culturas para representar comandos e funções .
* **Estruturas narrativas:** A estrutura de um programa Ella pode ser inspirada em histórias  e contos folclóricos ️ de diferentes culturas .
* **Sabedoria ancestral:** A linguagem Ella pode ser usada para codificar conhecimentos tradicionais , como medicina natural ‍⚕️, agricultura sustentável ‍ e práticas de cura .

Ao incorporar esses elementos, Ella celebra a riqueza e a diversidade das culturas do mundo  e reconhece a importância de preservar o conhecimento ancestral 🪶.

**Conclusão:**

Ella é mais do que uma linguagem de programação; é um movimento que promove a inclusão , a diversidade  e a preservação da cultura 🪶. É uma ferramenta poderosa  que pode ser usada para criar um futuro mais justo ⚖️ e sustentável  para todos ‍‍.


*  - Símbolo do mundo, representando a diversidade global
*  - Arco-íris, símbolo da inclusão e da diversidade
* ✨ - Brilho, representando a magia e a criatividade
* ️ - Fala, representando a comunicação e a expressão
*  - Paleta de cores, represent


# Inspiração


<p align="center">
  <img src="https://user-images.githubusercontent.com/50052600/196072481-d6070db0-93aa-4d6a-a9c7-e014c04ec27f.png" />
</p>

<h1 align="center">
 Libft
</h1>

<p align="center">
	<b><i>My first library in C : a collection of functions that will be useful tools for 42 Cursus.</i></b><br>
</p>

<p align="center">
	<img alt="GitHub code size in bytes" src="https://img.shields.io/github/languages/code-size/mewmewdevart/libft?color=6272a4" />
	<img alt="Main language" src="https://img.shields.io/github/languages/top/mewmewdevart/libft?color=6272a4"/>
	<img alt="Main language" src="https://img.shields.io/github/license/mewmewdevart/libft?color=6272a4"/>
</p>

## 💡 About the project

> _Building my own basic C static library, which will be used throughout the rest of the program (42 Cursus). The ```ft_*``` functions mostly mimic the behavior of the traditional C functions, and must be written using at most ```write()``` and ```malloc()```.

<p align="center">
	📌 In the premium libft folder is the most up-to-date version of the library with functions learned in others projects.
</p>


<p align="center">
	<a href="premium_libft">See my custom-made library (up up version)🚀🌎</a> 
</p>

## 🛠️ Usage

### Requirements

This project requires [GNU Compiler Collection](https://gcc.gnu.org/) and [GNU Make](https://www.gnu.org/software/make/) compiler. <br>
❗️| Make sure you have all the required tools installed on your local machine then continue with these steps.

### Instructions

**0. Download the archives**

Download the archives and go to the standard_library directory:

```bash
# Clone the repository
$ git clone https://github.com/mewmewdevart/libft
	
# Enter into the directory
$ cd libft/standard_libft/
```

**1. Compiling the library**

Run this command in your terminal :

```shell
$ make
```

**2. Using it in your code**

To use the library functions in your code, simply include its header:

```C
#include "libft.h"
```

If you try to compile your .c files with cc using "cc example.c", you will get an undefined symbol error for libft functions.
You have to tell the file which library it's using:  ↓

```shell
$ cc example.c libft.a -o prog_example
```

That's it. Now you can run it using ./prog_example .

## 📋 Credits

* [Acelera/Rodsmade](https://github.com/rodsmade/Projets_42_SP/)
* [libftTester/Tripouille](https://github.com/Tripouille/libftTester)
* [libft-war-machine/y3ll0w42](https://github.com/y3ll0w42/libft-war-machine)

<p align="center"> Developed with love 💜 by Larissa Cristina Benedito (Mewmew/Larcrist). </p>
